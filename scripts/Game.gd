extends Node2D

export var MAX_SCORE = 3

export(NodePath) var NODE_PATH_BALL
export(NodePath) var NODE_PATH_PADDLE_LEFT
export(NodePath) var NODE_PATH_PADDLE_RIGHT
export(NodePath) var NODE_PATH_GOAL_LEFT
export(NodePath) var NODE_PATH_GOAL_RIGHT
export(NodePath) var NODE_PATH_LABEL_SCORE_LEFT
export(NodePath) var NODE_PATH_LABEL_SCORE_RIGHT
export(NodePath) var NODE_PATH_RUNNING_CONTROL
export(NodePath) var NODE_PATH_PAUSED_CONTROL

onready var mBall = get_node(NODE_PATH_BALL)
onready var mPaddleLeft = get_node(NODE_PATH_PADDLE_LEFT)
onready var mPaddleRight = get_node(NODE_PATH_PADDLE_RIGHT)
onready var mGoalLeft = get_node(NODE_PATH_GOAL_LEFT)
onready var mGoalRight = get_node(NODE_PATH_GOAL_RIGHT)
onready var mLabelScoreLeft = get_node(NODE_PATH_LABEL_SCORE_LEFT)
onready var mLabelScoreRight = get_node(NODE_PATH_LABEL_SCORE_RIGHT)
onready var mRunningControl = get_node(NODE_PATH_RUNNING_CONTROL)
onready var mPausedControl = get_node(NODE_PATH_PAUSED_CONTROL)

signal reset
signal paused
var isPaused = false

var mLeftScore = 0
var mRightScore = 0

func _ready():
	update_scores()

func _physics_process(_delta):
	if(mBall.overlaps_area(mGoalLeft)):
		mRightScore += 1
		reset_game()
	if(mBall.overlaps_area(mGoalRight)):
		mLeftScore += 1
		reset_game()

func _input(event):
	if(event.is_action_pressed("ui_cancel")):
		isPaused = !isPaused
		if(isPaused): pause() 
		else: unpause()
		emit_signal("paused", isPaused)

func _process(_delta):
	if(mLeftScore == MAX_SCORE or mRightScore == MAX_SCORE):
		reset_game()
		return
	update_scores()

func update_scores():
	mLabelScoreLeft.text = str(mLeftScore)
	mLabelScoreRight.text = str(mRightScore)

func reset_game():
	emit_signal("reset")

func pause():
	mRunningControl.hide()
	mPausedControl.show()

func unpause():
	mPausedControl.hide()
	mRunningControl.show()
