extends Area2D

export(NodePath) var NODE_PATH_PADDLE_LEFT
export(NodePath) var NODE_PATH_PADDLE_RIGHT
export(NodePath) var NODE_PATH_WALL_TOP
export(NodePath) var NODE_PATH_WALL_BOTTOM

onready var mPaddleLeftArea = get_node(NODE_PATH_PADDLE_LEFT)
onready var mPaddleRightArea = get_node(NODE_PATH_PADDLE_RIGHT)
onready var mWallTopArea = get_node(NODE_PATH_WALL_TOP)
onready var mWallBottomArea = get_node(NODE_PATH_WALL_BOTTOM)

var mRand = RandomNumberGenerator.new()

var mDirection = Vector2()
var mSpeed = 10

var mDefaultPosX = position.x
var mDefaultPosY = position.y

var _Moving = true

func _ready():
	mDirection.x = mRand.randf() * mSpeed
	mDirection.y = mRand.randf() * mSpeed

func _physics_process(delta):
	if(!_Moving): return
	if(overlaps_area(mPaddleLeftArea) or overlaps_area(mPaddleRightArea)):
		mDirection.x *= -1
	if(overlaps_area(mWallTopArea) or overlaps_area(mWallBottomArea)):
		mDirection.y *= -1
	position += mDirection * mSpeed * delta

func _on_Game_paused(is_paused):
	_Moving = !is_paused

func _on_Game_reset():
	position.x = mDefaultPosX
	position.y = mDefaultPosY
