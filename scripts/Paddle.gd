extends Area2D

const SPEED = 100

enum MoveDirection { Up, Down, None }

export(String) var INPUT_ACTION_UP
export(String) var INPUT_ACTION_DOWN

var mDefaultPosX = position.x
var mDefaultPosY = position.y

var _mHandlingMoves = true

func _physics_process(delta):
	var move_direction = MoveDirection.None
	if Input.is_action_pressed(INPUT_ACTION_UP):
		move_direction = MoveDirection.Up
	if Input.is_action_pressed(INPUT_ACTION_DOWN):
		move_direction = MoveDirection.Down
	move(delta, move_direction)

func move(delta, direction):
	if(!_mHandlingMoves): return
	
	var dir = Vector2(0, 0)
	if(direction == MoveDirection.Up): 
		dir.y = -1
	if(direction == MoveDirection.Down):
		dir.y = 1
	position += dir * SPEED * delta

func _on_Game_paused(is_paused):
	_mHandlingMoves = !is_paused

func _on_Game_reset():
	position.x = mDefaultPosX
	position.y = mDefaultPosY
